package com.imooc.security.core.social;

import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SpringSocialConfigurer;

public class ImoocSpringSocialConfig  extends SpringSocialConfigurer{
  
	private String filterProcessUrl;
	
	public ImoocSpringSocialConfig(String filterProcessUrl) {
		this.filterProcessUrl=filterProcessUrl;
	}
	
	/**
	 * 传入的参数就是要加进过滤器链上的filter
	 */
	@Override
	protected <T> T postProcess(T object) {
		SocialAuthenticationFilter social=(SocialAuthenticationFilter) super.postProcess(object);
		social.setFilterProcessesUrl(filterProcessUrl);
		return (T) social;
	}
}
