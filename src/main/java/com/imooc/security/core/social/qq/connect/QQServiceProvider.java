  
    
package com.imooc.security.core.social.qq.connect;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

import com.imooc.security.core.social.qq.api.QQ;
import com.imooc.security.core.social.qq.api.QQImpl;

/**  
     * 此类描述的是：  
     * @author: wangyingxu

     * @version: 2018年3月14日 下午4:53:50  
 * @param <S>
     */

public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQ> {

	private String appId;
	
	private static final String URL_AUTHORIZE="https://graph.qq.com/oauth2.0/authorize";
	
	private static final String URL_ACCESS_TOKEN ="https://graph.qq.com/oauth2.0/token";
	
	
	/**
	 * 
	     * 创建一个新的实例 QQServiceProvider.  
	     *  clientId实际上就是appId 
	     *  clientSecret是appSecret 
	     *  authorizeUrl导向服务器的时候导向的url
	     *  accessTokenUrl申请令牌时候的地址
	     *  这两个参数是注册QQ互联获取的
	     * @param oauth2Operations
	 */
	public QQServiceProvider(String appId,String appSecret) {
		super(new QQOAuth2Template(appId, appSecret, URL_AUTHORIZE, URL_ACCESS_TOKEN));
		this.appId=appId;
	}

	
	
	@Override
	public QQ getApi(String accessToken) {
		return new QQImpl(accessToken, appId);
	}

}
