  
    /**  
     * 文件名：QQImpl.java  
     *  
     * 版本信息：  
     * 日期：2018年3月14日  
     * Copyright 足下 Corporation 2018  
     * 版权所有  
     *  
     */  
    
package com.imooc.security.core.social.qq.api;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.oauth2.TokenStrategy;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**  
     * 此类描述的是：  
     * @author: wangyi

     * @version: 2018年3月14日 下午4:11:46  
     */

public class QQImpl extends AbstractOAuth2ApiBinding implements QQ {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	private static final String URL_GET_OPENID="https://graph.qq.com/oauth2.0/me?access_token=%s";

	private static final String URL_GET_USERINFO="https://graph.qq.com/user/get_user_info?oauth_consumer_key=%s&openid=%s";
	
	private String appId;

	private String openId;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	
	public QQImpl(String accessToken,String appId) {
		super(accessToken,TokenStrategy.ACCESS_TOKEN_PARAMETER);
		this.appId=appId;
		String url=String.format(URL_GET_OPENID, accessToken);
		String result=getRestTemplate().getForObject(url, String.class);
		logger.info("QQImpl获取的结果集是"+result);
		this.openId=StringUtils.substringBetween(result,"\"openid\":\"", "\"}");
		logger.info("QQImpl获取的opneID是"+this.openId);
		
	}

	/* (non-Javadoc)  
	 * @see com.imooc.security.core.social.qq.api.QQ#getUserInfo()  
	 */

	@Override
	public QQUserInfo getUserInfo(){
		String url=String.format(URL_GET_USERINFO, appId,openId);
		String result=getRestTemplate().getForObject(url, String.class);
		logger.info("QQImpl获取的用户信息是是"+result);
		QQUserInfo userInfo=null;
		try {
			userInfo= objectMapper.readValue(result, QQUserInfo.class);
			userInfo.setOpenId(openId);
			return userInfo;
		} catch (Exception e) {
			throw new RuntimeException("获取用户信息失败");
		} 
	}

}
