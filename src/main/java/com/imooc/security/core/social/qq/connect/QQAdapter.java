package com.imooc.security.core.social.qq.connect;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

import com.imooc.security.core.social.qq.api.QQ;
import com.imooc.security.core.social.qq.api.QQUserInfo;
 
/**
 *   这个泛型指的就是这个适配器所要适配的api的类型是什么
     * 此类描述的是：  
     * @author: wangyingxu

     * @version: 2018年3月14日 下午5:09:24
 */
public class QQAdapter implements ApiAdapter<QQ> {

	/**
	 * 测试当前的api是否可用的,默认它是通的 直接返回true
	 */
	@Override
	public boolean test(QQ api) {
		return true;
	}

	/**
	 * 把ConnectionValues需要的数据设置上 包含了创建一个connectFactory需要的数据项
	 */
	@Override
	public void setConnectionValues(QQ api, ConnectionValues values) {
		//拿到QQ的用户信息
		try {
			QQUserInfo userInfo=api.getUserInfo();
			values.setDisplayName(userInfo.getNickname());
			values.setImageUrl(userInfo.getFigureurl_qq_1());
			values.setProfileUrl(null);
			values.setProviderUserId(userInfo.getOpenId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public UserProfile fetchUserProfile(QQ api) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateStatus(QQ api, String message) {
		// TODO do nothing
	}

}
