package com.imooc.security.core.validate.code;

import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 
 * @author 0
 *
 */
public class ImageCode {

	
	private BufferedImage image;
	
	private String code;
	
	private LocalDateTime expireTime;
	
	
	public ImageCode(BufferedImage bufferedImage, String code, int expireIn) {
		this.image = bufferedImage;
		this.code = code;
		this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
	}
	

	public ImageCode(BufferedImage bufferedImage, String code, LocalDateTime expireTime) {
		this.image = bufferedImage;
		this.code = code;
		this.expireTime = expireTime;
	}
	
	public boolean isExpried() {
		return LocalDateTime.now().isAfter(expireTime);
	}


	public BufferedImage getImage() {
		return image;
	}


	public void setImage(BufferedImage image) {
		this.image = image;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public LocalDateTime getExpireTime() {
		return expireTime;
	}


	public void setExpireTime(LocalDateTime expireTime) {
		this.expireTime = expireTime;
	}
	
}
