package com.imooc.security.core.validate.code.sms;


/**
 * 短信验证码默认实现
 * @author 0
 */
public class DefaultSmsCodeSender implements SmsCodeSender {

	@Override
	public void send(String mobile, String code) {
		System.out.println("向手机"+mobile+"发送短信验证码"+code);
	}

}
