package com.imooc.security.core.validate.code;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 登录验证码控制层
 * @author 0
 *
 */
@RestController
public class ValidateCodeController {
	
	@Autowired
	private Map<String, ValidateCodeProcessor> validateCodeProcessors;
	
	/**
	 * 创建验证码，根据验证码类型不同，调用不同的ValidateCodeProcessor接口实现
	 * @throws Exception 
	 */
	
	@GetMapping("/code/{type}")
	public void createCode(HttpServletRequest request,HttpServletResponse response,@PathVariable String type) throws Exception {
		validateCodeProcessors.get(type+"CodeProcessor").create(new ServletWebRequest(request,response));
	}
	
	
}
