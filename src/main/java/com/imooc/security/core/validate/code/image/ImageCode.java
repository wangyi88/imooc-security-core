package com.imooc.security.core.validate.code.image;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

import com.imooc.security.core.validate.code.ValidateCode;

/**
 * 图片验证码基础类，因为短信验证码和图片属性有两个一致 所以继承它
 * @author 0
 *
 */
public class ImageCode  extends ValidateCode{
	
	private BufferedImage image;
	
	
	public ImageCode(BufferedImage bufferedImage, String code, int expireIn) {
		super(code, expireIn);
		this.image = bufferedImage;
	}
	

	public ImageCode(BufferedImage bufferedImage, String code, LocalDateTime expireTime) {
		super(code, expireTime);
		this.image = bufferedImage;
	}
	

	public BufferedImage getImage() {
		return image;
	}


	public void setImage(BufferedImage image) {
		this.image = image;
	}

	
}
