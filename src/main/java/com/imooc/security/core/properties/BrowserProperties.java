package com.imooc.security.core.properties;


/**
 * 
 * @author 0
 *
 */
public class BrowserProperties {
	
	//设置默认的登录页面
	private String loginPage="/imoocLogin.html";
	
	private String signUpUrl="/imooc-signUpUrl.html";
	
	//设置默认的登录方式
	private LoginType loginType=LoginType.JSON;
	
	//设置记住我的时间
	private int  remeberMeSeconds=3600;
	
	
	public LoginType getLoginType() {
		return loginType;
	}

	public void setLoginType(LoginType loginType) {
		this.loginType = loginType;
	}

	public String getLoginPage() {
		return loginPage;
	}

	public void setLoginPage(String loginPage) {
		this.loginPage = loginPage;
	}

	public int getRemeberMeSeconds() {
		return remeberMeSeconds;
	}

	public void setRemeberMeSeconds(int remeberMeSeconds) {
		this.remeberMeSeconds = remeberMeSeconds;
	}

	public String getSignUpUrl() {
		return signUpUrl;
	}

	public void setSignUpUrl(String signUpUrl) {
		this.signUpUrl = signUpUrl;
	}
	
}
