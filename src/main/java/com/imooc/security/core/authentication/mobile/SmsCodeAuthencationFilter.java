package com.imooc.security.core.authentication.mobile;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

public class SmsCodeAuthencationFilter extends AbstractAuthenticationProcessingFilter {
	
	public static final String IMOOC_FORM_MOBILE_KEY = "mobile";
	
	private String mobileParameter = IMOOC_FORM_MOBILE_KEY;
	private boolean postOnly = true;
	
	// ~ Constructors
	// ===================================================================================================
	
	public SmsCodeAuthencationFilter() {
		super(new AntPathRequestMatcher("/authentication/mobile", "POST"));
	}
	
	// ~ Methods
	// ========================================================================================================
	
	public Authentication attemptAuthentication(HttpServletRequest request,
		HttpServletResponse response) throws AuthenticationException {
		if (postOnly && !request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException(
					"Authentication method not supported: " + request.getMethod());
		}
		
		String mobile = obtainMobile(request);
		
		if (mobile == null) {
			mobile = "";
		}
		mobile = mobile.trim();
		 
		SmsCodeAuthenticationToken authRequest = new SmsCodeAuthenticationToken(
				mobile);
		
		// Allow subclasses to set the "details" property
		setDetails(request, authRequest);
		return this.getAuthenticationManager().authenticate(authRequest);
	}
	
	
	/**
	* 获取手机号的方法
	* request token to the <code>AuthenticationManager</code>
	*/
	protected String obtainMobile(HttpServletRequest request) {
		return request.getParameter(mobileParameter);
	}
	
	/**
	 * 把请求的详情比如说sessonId、IP设置到验证请求里面去
	* @param authRequest the authentication request object that should have its details
	* set
	*/
	protected void setDetails(HttpServletRequest request,
		SmsCodeAuthenticationToken authRequest) {
		authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
	}
	
	/**
	* Sets the parameter name which will be used to obtain the username from the login
	* request.
	*
	* @param usernameParameter the parameter name. Defaults to "username".
	*/
	public void setMobileParameter(String mobileParameter) {
		Assert.hasText(mobileParameter, "Username parameter must not be empty or null");
		this.mobileParameter = mobileParameter;
	}
	
	
	/**
	* Defaults to <tt>true</tt> but may be overridden by subclasses.
	*/
	public void setPostOnly(boolean postOnly) {
		this.postOnly = postOnly;
	}
	
	public final String getMobileParameter() {
		return mobileParameter;
	}
	
}
